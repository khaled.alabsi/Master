package services;

import java.util.HashMap;
import java.util.Map;
import models.FindFrequenciesResult;

public class Frequencies {


    FindFrequenciesResult FindFrequencies(String str, char[] BuchstabenMenge) {

        Map<String, Integer> map = new HashMap<String, Integer>();
        int max = 0;
        char maximalChar = 'n';
        int index = 0;
        FindFrequenciesResult findFrequenciesResult = new FindFrequenciesResult('n', 0, 0);
        //str.chars().filter(num -> num == '$').count()
        for (int i = 0; i < BuchstabenMenge.length; i++) {
            String tmp = "";
            int t = 0, j = 0;

            while (tmp == "" || t != -1) {
                tmp += String.valueOf(BuchstabenMenge[i]);
                t = str.indexOf(new StringBuilder().append(tmp).append(String.valueOf(BuchstabenMenge[i])).toString());
                j++;
            }
            if (j > findFrequenciesResult.getFreq()) {
                findFrequenciesResult.setMaximalChar(BuchstabenMenge[i]);
                findFrequenciesResult.setFreq(j);
                findFrequenciesResult.setIndex(str.indexOf(tmp));

            }
            map.put(String.valueOf(BuchstabenMenge[i]), j);
        }
        System.out.println(map);
        System.out.println(findFrequenciesResult);

        return findFrequenciesResult;
    }
}
