package services;

import models.MatcherResult;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

public class MatchingOps {


    public MatchingOps() {

    }

    public int bestMatch(char[] BuchstabenMenge,String chiffertext, String fragment) {
        List<MatcherResult> resultes = new ArrayList<MatcherResult>();
        // get all Matches
        for (int i = 0; i < BuchstabenMenge.length; i++) {
            resultes.add(matchFragmentToChiffer(BuchstabenMenge, chiffertext, fragment, i));
            //resultes.add(matchFragmentToChiffer(BuchstabenMenge, "eeeeaaaabbbeaaaa", "beeeeeee",i));
        }
        // find the best
        MatcherResult maxValue = resultes.stream().max(Comparator.comparing(v -> v.getBestMatchesScore())).get();
        System.out.println("beste Treffer :");
        System.out.println(maxValue);
        //System.out.println( chiffertext.substring(maxValue.getIndexForBestMatchesScore(),maxValue.getIndexForBestMatchesScore()+fragment.length()));
        return maxValue.getIndexForBestMatchesScore();
    }


    MatcherResult matchFragmentToChiffer(char[] letters, String chiffertxt, String fragmanet, int ShiftTo) {
        MatcherResult matcherResult = new MatcherResult();
        int[] map;

        for (int index = 0; index + fragmanet.length() <= chiffertxt.length(); index++) {
            int matchesSum = 0;
            map = new int[fragmanet.length()];
            for (int i = 0; i < fragmanet.length(); i++) {

                map[i] = shiftCharTo(fragmanet.charAt(i), ShiftTo, letters) == chiffertxt.charAt(i + index) ? 1 : 0;
                //System.out.println( fragmanet.charAt(i)+ " =? "+chiffertxt.charAt(i + index)+ " :: "+ map[i]);
            }
            matchesSum = IntStream.of(map).sum();
            if (matchesSum > matcherResult.getBestMatchesScore()) {
                matcherResult.setBestMatchesScore(matchesSum);
                matcherResult.setIndexForBestMatchesScore(index);
                matcherResult.setShifting(ShiftTo);

            }
        }
        System.out.println(matcherResult);
        //System.out.println(chiffertxt.substring(matcherResult.getIndexForBestMatchesScore(), matcherResult.getIndexForBestMatchesScore() + fragmanet.length()));

        //generateKey(BuchstabenMenge, chiffertxt.substring(matcherResult.getIndexForBestMatchesScore(), matcherResult.getIndexForBestMatchesScore() + fragmanet.length()), fragmanet);
        //generateKey(BuchstabenMenge, "acebdibbee", "ababcebaae");

        //System.out.println( fragmanet);
        return matcherResult;
    }

    char shiftCharTo(char ch, int to, char[] letters) {

        for (int i = 0; i < letters.length; i++) {

            if (ch == letters[i]) {

                return letters[(i + to) % letters.length];
            }
        }
        return 'x';
    }
}
