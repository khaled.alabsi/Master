package models;

public class FindFrequenciesResult {
    private char maximalChar;
    private int freq;
    private int index;

    public char getMaximalChar() {
        return maximalChar;
    }

    public int getFreq() {
        return freq;
    }

    public int getIndex() {
        return index;
    }

    public FindFrequenciesResult(char maximalChar, int freq, int index) {
        this.maximalChar = maximalChar;
        this.freq = freq;
        this.index = index;
    }

    @Override
    public String toString() {
        return "FindFrequenciesResult{" +
                "maximalChar=" + maximalChar +
                ", freq=" + freq +
                ", index=" + index +
                '}';
    }

    public void setMaximalChar(char maximalChar) {
        this.maximalChar = maximalChar;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}