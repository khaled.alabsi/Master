package views;

public class view {


    public static String getHeader() {
        String header = "";
        header += formatDiv("a-----b-------------b----------b----------------------b----------------------b---------------------b---------------------b--------------b-----------------b---------b------------c\n");
        header += formatRow("|  b  | Iterationen | zulässig | Wiederholte Tabellen | Netto ohne Bedienung | Netto mit Bedienung | Standard Abweichung | Durchschnitt | log(Netto) bits | b) Bits | Abweichung |\n");
        header += formatDiv("d-----e-------------e----------e----------------------e----------------------e---------------------e---------------------e--------------e-----------------e---------e------------f\n");
        return header;
    }

    public static String formatDiv(String str) {
        return str.replace('a', '\u250c')
                .replace('b', '\u252c')
                .replace('c', '\u2510')
                .replace('d', '\u251c')
                .replace('e', '\u253c')
                .replace('f', '\u2524')
                .replace('g', '\u2514')
                .replace('h', '\u2534')
                .replace('i', '\u2518')
                .replace('-', '\u2500');
    }

    public static int primeEquation(int x) {
        return (x * x) - x + 41;
    }

    public static String formatRow(String str) {
        return str.replace('|', '\u2502');
    }
}
