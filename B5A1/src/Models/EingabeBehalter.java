package Models;

import java.math.BigInteger;
import java.util.Arrays;

public class EingabeBehalter {

    BigInteger a;
    Boolean[] b;
    BigInteger n;

    public EingabeBehalter(BigInteger a, Boolean[] b, BigInteger n) {
        this.a = a;
        this.b = b;
        this.n = n;
    }

    public EingabeBehalter() {
    }

    public BigInteger getA() {
        return a;
    }

    public Boolean[] getB() {
        return b;
    }

    public BigInteger getN() {
        return n;
    }

    public void setA(BigInteger a) {
        this.a = a;
    }

    public void setB(Boolean[] b) {
        this.b = b;
    }

    public void setN(BigInteger n) {
        this.n = n;
    }

    @Override
    public String toString() {
        return "Models.EingabeBehalter{" +
                "a=" + a +
                ", b=" + Arrays.toString(b) +
                ", n=" + n +
                '}';
    }
}
