package Models;

public class ResultsBehalter {
    private int aLength;
    private int bLength;
    private int nLength;
    private Long rechnenZeitInMS;
    private Long rechnenZeitInforStandard;

    public ResultsBehalter() {
    }

    public ResultsBehalter(int aLength, int bLength, int nLength, Long rechnenZeitInMS) {
        this.aLength = aLength;
        this.bLength = bLength;
        this.nLength = nLength;
        this.rechnenZeitInMS = rechnenZeitInMS;
    }

    public ResultsBehalter(int aLength, int bLength, int nLength) {
        this.aLength = aLength;
        this.bLength = bLength;
        this.nLength = nLength;
    }

    public ResultsBehalter(int aLength, int bLength, Long rechnenZeitInMS, Long rechnenZeitInforStandard) {
        this.aLength = aLength;
        this.bLength = bLength;
        this.rechnenZeitInMS = rechnenZeitInMS;
        this.rechnenZeitInforStandard = rechnenZeitInforStandard;
    }

    public int getaLength() {
        return aLength;
    }

    public void setaLength(int aLength) {
        this.aLength = aLength;
    }

    public int getbLength() {
        return bLength;
    }

    public void setbLength(int bLength) {
        this.bLength = bLength;
    }

    public int getnLength() {
        return nLength;
    }

    public void setnLength(int nLength) {
        this.nLength = nLength;
    }

    public Long getRechnenZeitInMS() {
        return rechnenZeitInMS;
    }

    public void setRechnenZeitInMS(Long rechnenZeitInMS) {
        this.rechnenZeitInMS = rechnenZeitInMS;
    }

    @Override
    public String toString() {
        return "ResultsBehalter{" +
                "aLength=" + aLength +
                ", bLength=" + bLength +
                ", nLength=" + nLength +
                ", rechnenZeitInMS=" + rechnenZeitInMS +
                '}';
    }

    public Long getRechnenZeitInforStandard() {
        return rechnenZeitInforStandard;
    }

    public void setRechnenZeitInforStandard(Long rechnenZeitInforStandard) {
        this.rechnenZeitInforStandard = rechnenZeitInforStandard;
    }
}
