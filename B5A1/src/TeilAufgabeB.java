import Models.ResultsBehalter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TeilAufgabeB {

    TeilAufgabeA funcktionen = new TeilAufgabeA();

    public void run() {
        System.out.println("***************************************************************************************************************");
        System.out.println("*                                                                                                             *");
        System.out.println("*                                   TeilAufgabe (b                                                            *");
        System.out.println("*                                                                                                             *");
        System.out.println("***************************************************************************************************************");
        List<ResultsBehalter> resultsList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            int maximum = 100000;
            int minimum = 1000;
            Random rn = new Random();
            int range = maximum - minimum + 1;
            int randomNum =  rn.nextInt(range) + minimum;
            BigInteger a = new BigInteger(funcktionen.getRandomNumber(randomNum));
            randomNum =  rn.nextInt(range) + minimum;
            BigInteger b = new BigInteger(funcktionen.getRandomNumber(randomNum));
            long startTime = System.currentTimeMillis();
            new Karatsuba().karatsuba(a, b);
            long rechenzeitKaratsuba = System.currentTimeMillis() - startTime;
            startTime = System.currentTimeMillis();
            a.multiply(b);
            long rechenzeitStandard = (System.currentTimeMillis() - startTime);
            System.out.print("rechenzeitKaratsuba: "+rechenzeitKaratsuba + "::" +"rechenzeitStandard: "+ rechenzeitStandard + "\r");
            resultsList.add(new ResultsBehalter(a.toString().length(), b.toString().length(), rechenzeitKaratsuba, rechenzeitStandard));
        }
        printResults(resultsList);
    }

    void printResults(List<ResultsBehalter> results) {
        TableList tl = new TableList(5,
                "a.Length",
                "b.Length",
                "Rechenzeit fuer Karatsuba",
                "Rechenzeit fuer Standrad",
                "Unterschied").withUnicode(true);

        long[] durchschnittKaratsuba = new long[results.size()] ;
        long[] durchschnittStandrard = new long[results.size()] ;
        for (int i = 0; i < results.size(); i++) {
            durchschnittKaratsuba[i]= results.get(i).getRechnenZeitInMS();
            durchschnittStandrard[i]= results.get(i).getRechnenZeitInforStandard();
        }
        results.forEach(obj -> tl.addRow(
                String.valueOf(obj.getaLength()),
                String.valueOf(obj.getbLength()),
                String.valueOf(obj.getRechnenZeitInMS()),
                String.valueOf(obj.getRechnenZeitInforStandard()),
                String.valueOf(obj.getRechnenZeitInMS() - obj.getRechnenZeitInforStandard())
        ));
        tl.addRow("--------","--------","----------------","----------------","--------");
        int dk= (int)Arrays.stream(durchschnittKaratsuba).average().orElse(-1);
        long ds = (int)Arrays.stream(durchschnittStandrard).average().orElse(-1);
        tl.addRow("--------","Durchschnitt",String.valueOf(dk),String.valueOf(ds),String.valueOf((dk-ds)));
        tl.print();

    }

}
