import Models.EingabeBehalter;
import Models.ResultsBehalter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TeilAufgabeA {

    int defaultLength = 1000;

    public void run() {
        System.out.println("***************************************************************************************************************");
        System.out.println("*                                                                                                             *");
        System.out.println("*                                   TeilAufgabe (a                                                            *");
        System.out.println("*                                                                                                             *");
        System.out.println("***************************************************************************************************************");

        einflussVon('b',false);
        einflussVon('a',false);
        einflussVon('n',false);
    }

    public void einflussVon(char untersuchteGrosse,boolean showDetails) {
        //Setting
        // wieviel Runde und wievielmal soll eine zufällige Zahl per Runde erzeugt werden.
        // Bei jede Runde ist der Anzahl der Bits festbleibet aber die Befüllung verändert sich (howManyRandomValuePerOneIncrement) mal per Runde (numberOfIncrements).
        // aus jedem Runde wird der Durchschnitt der Rechenzeit berechnet.
        int numberOfIncrements = 10;
        int howManyRandomValuePerOneIncrement = 10;
        int additionFactor = 1000; // um wieviel vergroßert sich den Lenght per Runde
        List<ResultsBehalter> resultsBehalterList = new ArrayList<>();
        int tmpAdditionFactor = 0;

        for (int i = 1; i <= numberOfIncrements; i++) {
            if(showDetails) System.out.println("Eingabe vorberieten...   ");
            tmpAdditionFactor += additionFactor;
            List<EingabeBehalter> eingabeList = prepareInput(howManyRandomValuePerOneIncrement, tmpAdditionFactor, untersuchteGrosse);
            if(showDetails) System.out.println("Runde " + i + "/" + numberOfIncrements + " starten ..");
            long[] elapsedTimeList = new long[howManyRandomValuePerOneIncrement];
            for (int j = 0; j < eingabeList.size(); j++) {
                long startTime = System.currentTimeMillis();
                potenz(eingabeList.get(j).getA(), eingabeList.get(j).getB(), eingabeList.get(j).getN());
                elapsedTimeList[j] = System.currentTimeMillis() - startTime;
            }
            System.out.print("Einfluess von "+ untersuchteGrosse + " auf die Rechenzeit -> Runde : " + i + "/" + eingabeList.size() + "  "+" Durschnitt : " + Arrays.stream(elapsedTimeList).average().orElse(-1)+ "\r");
            ResultsBehalter resultsBehalter = new ResultsBehalter(eingabeList.get(i - 1).getA().toString().length(), eingabeList.get(i - 1).getB().length, eingabeList.get(i - 1).getN().toString().length(), (long) Arrays.stream(elapsedTimeList).average().orElse(-1));
            if(showDetails) System.out.println("Durchschnitt Rechnenzeit: " + Arrays.stream(elapsedTimeList).average().orElse(-1));
            resultsBehalterList.add(resultsBehalter);

        }
        printToUser(resultsBehalterList, untersuchteGrosse,showDetails);
    }

    private void printToUser(List<ResultsBehalter> resultsBehalterList, char untersuchteGrosse,boolean showDetails) {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println();
        if(showDetails)System.out.println("Results fuer " + untersuchteGrosse);
        if(showDetails) resultsBehalterList.forEach(o -> System.out.println(o));
        System.out.println("Bitte Kopieren Sie diese Ausgabe zu Exel");
        System.out.println();
        System.out.println(untersuchteGrosse);
        resultsBehalterList.forEach(o -> {
            if (untersuchteGrosse == 'a') System.out.println(o.getaLength());
            if (untersuchteGrosse == 'b') System.out.println(o.getbLength());
            if (untersuchteGrosse == 'n') System.out.println(o.getnLength());
        });
        System.out.println();
        System.out.println("Rechenzeit");
        resultsBehalterList.forEach(o -> System.out.println(o.getRechnenZeitInMS()));
        System.out.println("--------------------------------------------------------------------------------");
    }


    public List<EingabeBehalter> prepareInput(int numberOfInputs, int additionFactor, char untersuchteGrosse) {
        List<EingabeBehalter> eingabeList = new ArrayList<>();
        for (int i = 1; i <= numberOfInputs; i++) {
            eingabeList.add(new EingabeBehalter(
                    new BigInteger(getRandomNumber((defaultLength- 10) + (i>1 ?(untersuchteGrosse == 'a' ? additionFactor : 0): 0) )), // -10 Stellen somit ist a<n
                    getRandomBooleanArray(defaultLength + (i>1 ?(untersuchteGrosse == 'b' ? additionFactor : 0) : 0)),
                    new BigInteger(getRandomNumber(defaultLength + (i>1 ?(untersuchteGrosse == 'n' ? additionFactor : 0): 0)))
            ));
        }
        return eingabeList;
    }

    public BigInteger potenz(BigInteger a, Boolean[] b, BigInteger n) {
        BigInteger p = a;
        for (int i = 1; i < b.length; i++) {
            p = (p.multiply(p)).mod(n);
            if (b[i]) p = p.multiply(a).mod(n);
        }
        //System.out.println(p);
        return p;
    }

    public Boolean[] convertBitArraytoBooleanArray(int[] bits) {
        Boolean result[] = new Boolean[bits.length];
        for (int i = 0; i < bits.length; i++) {
            result[i] = bits[i] == 1 ? true : false;
        }

        return result;
    }


    public Boolean[] getRandomBooleanArray(int length) {
        Boolean[] arr = new Boolean[length];
        Random randomGenerator = new Random();
        for (int i = 0; i < length; i++) {
            arr[i] = randomGenerator.nextBoolean();
        }

        return arr;
    }

    public String getRandomNumber(int length) {
        StringBuilder randomNumber = new StringBuilder();
        Random rand = new Random();
        for (int i = 0; i < length; i++) {
            randomNumber.append(rand.nextInt(10));
        }
        return randomNumber.toString();
    }


}

