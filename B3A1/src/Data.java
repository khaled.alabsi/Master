public class Data {

    public String chiffreText  = "1001010011011001001011001111110110101111000111001110010110100101000010101111111111101010011000111001011000001011011110110101111";
    public String klarText = "0101001101010010101011100101010001011111101010101111110101010100011110101010101011010100011101010101010100010101010101010101010";
    //public int[] KoeffizientenC={0,0,0,0,1,1,0,1,1};
    public Boolean[] koeffizientenC={true,false,false,false,true,true,false,true,true};
    public int startIndex= 12;
    public Boolean[] TestKoeffizientenC={true,false,true,true,true};
    public Boolean[] testSeed={false,true,true,false};

    public Boolean[] getClearText() {
        Boolean[] bits = new Boolean[klarText.length()];
        for (int i = 0; i < bits.length; i++) {
            bits[i] =  "1".equals(String.valueOf(klarText.charAt(i)));
        }
        return bits;
    }

    public Boolean[] getcipherText() {
        Boolean bits[] = new Boolean[chiffreText.length()];
        for (int i = 0; i < bits.length; i++) {
            bits[i] =  "1".equals(String.valueOf(chiffreText.charAt(i)));
        }
        return bits;
    }
}
