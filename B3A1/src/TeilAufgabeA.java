public class TeilAufgabeA {

    public TeilAufgabeA() {
    }

    public void run() {
        Services services = new Services();
        Data data = new Data();
        LFSR lfsr = new LFSR();

//** TeilAufgabe (a
        System.out.println("***************************************************************************************************************");
        System.out.println("*                                                                                                             *");
        System.out.println("*                                   TeilAufgabe (a                                                            *");
        System.out.println("*                                                                                                             *");
        System.out.println("***************************************************************************************************************");
        // s12+i = ci ⊕ ki
        System.out.println("Die Folge der für die Verschlüsselung verwendeten Bits aus dem Klartext und dem Chiffretext gewinnen: ");
        services.getKeyAsInt(data.getClearText(), data.getcipherText());
        System.out.println("Schritt 2: Der Schluessel/Seed ausrechnen: ");
        String key = lfsr.convertBoolenArrayToString(services.getKeyAsBooleanArray(data.getClearText(), data.getcipherText()));
        String seed = lfsr.calculateBackward(data.koeffizientenC, services.getKeyAsBooleanArray(data.getClearText(), data.getcipherText()), data.startIndex);
        System.out.println("Der Schlüssel: " + seed);
        System.out.println("");
        System.out.println("Schritt 3: LFSR mit dem koeffizienten C bilden ");
        lfsr.build(data.koeffizientenC);
        System.out.println("");
        System.out.println("Schritt 4: LFSR mit dem gerechnette Seed für 127+12 Takt und Aufwaermphase 12 starten ");
        System.out.println("");
        Boolean[] LFSRoutPut = lfsr.run(lfsr.convertStringToBoolenArray(seed), 127 + 12, 12);
        String LFSRoutPutString = lfsr.convertBoolenArrayToString(LFSRoutPut);
        System.out.println(LFSRoutPutString);
        System.out.println("");
        System.out.println("Schritt 5: Ausgabe das LFSR mit dem Schluessel aus 1 vergleichen ");
        if (key.equals(LFSRoutPutString)) {
            System.out.println("Ja! sie sind identisch");
        } else System.out.println("sie sind leider nicht identisch");
        System.out.println("");
        System.out.println(" Man kann auch die die Feedback Polynom aus den koeffizienten C rechnen:");
        lfsr.feedbackPolynom(data.koeffizientenC);
        System.out.println("");
    }
}
