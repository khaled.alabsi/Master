import java.security.PublicKey;
import java.util.Arrays;

public class LFSR {


    private int n;
    private Boolean[] rules;
    private Boolean[] results;
    private Boolean[] LFSR;


    public void build(Boolean[] LFSRKoeffizienten) {
        // wie groß ist der LFSR  S_n
        n = LFSRKoeffizienten.length - 1;
        rules = new Boolean[n];
        for (int i = 0; i < n; i++) {
            rules[i] = LFSRKoeffizienten[i];
        }

    }

    public Boolean[] TestKoeffizientenC = {true, false, true, true, true};

    public Boolean[] run(Boolean[] seed, long takt, int warmUpPhase) {
        if (n != seed.length) {
            System.out.println("Der Seed ist groeßere als S_n");
            System.exit(-1);
        }
        results = new Boolean[(int) takt];
        LFSR = seed;
        for (long i = 0; i < takt; i++) {
            //
            results[(int) i] = LFSR[0];
            // neue Rn  berechnen
            Boolean tmp = false;
            for (int Ri = 0; Ri < n; Ri++) {
                if (rules[Ri]) tmp = tmp ^ LFSR[Ri];
            }
            // verschiebung
            for (int Si = 0; Si < n - 1; Si++) {
                LFSR[Si] = LFSR[Si + 1];
            }
            LFSR[n - 1] = tmp;
        }

        Boolean[] resultsWithWarmUp = new Boolean[((int) takt) - warmUpPhase];
        for (int i = warmUpPhase, j = 0; i < results.length; i++, j++) {
            int r = results[i] ? 1 : 0;
            resultsWithWarmUp[j] = results[i];
            // System.out.print(r);
        }

        return resultsWithWarmUp;
    }

    public Boolean[] convertStringToBoolenArray(String seed) {
        Boolean[] boolenArray = new Boolean[seed.length()];
        for (int i = 0; i < boolenArray.length; i++) {
            boolenArray[i] = "1".equals(String.valueOf(seed.charAt(i)));
        }

        return boolenArray;
    }

    public String convertBoolenArrayToString(Boolean[] tmp) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < tmp.length; i++) {
            int value = tmp[i] ? 1 : 0;
            result.append(String.valueOf(value));
        }
        return String.valueOf(result);
    }

    public Boolean[] convertFeedBackPolynomToCoefficientsArray(String FeedBackPolynom) {
        FeedBackPolynom = FeedBackPolynom.replaceAll("\\s+","");
        String[] parts = (FeedBackPolynom.replace("+1", "")).split("\\+");
        int[] powers = new int[parts.length];
        int i = 0;
        for (String part : parts) {
            if (part.contains("^")) {
                part = part.replace("x", "");
                part = part.replace("^", "");
                powers[i] = Integer.parseInt(part);
                i++;
            } else if (part.contains("x")) {
                part = part.replace("x", "1");
                powers[i] = Integer.parseInt(part);
                i++;
            }
            //System.out.println(part);
        }
        Arrays.sort(powers);


        //wieviel C haben wir? der großter grad von x, der sich in letzten Index befindet
        Boolean[] results = new Boolean[powers[powers.length-1]+1];
        for (int index = 0; index < results.length; index++) {
            results[index] = false;
        }
        results[0] = true; //1 falls C_i=0 ist
        results[results.length - 1] = true; //1 falls C_i=n ist
        // man braucht die Hochste grad von x nicht mehr.Es muss entfernt werden
        int[] newPowers = new int[powers.length-1];
        for (int i1 = 0; i1 < newPowers.length; i1++) {
            newPowers[i1] = powers[i1];
        }

        for (int g = 0; g < newPowers.length; g++) {
            int where = powers[powers.length-1] - newPowers[g];
            results[ where]= true;
        }
 /*       for (Boolean result : results) {
            System.out.print(result + " ");
        }
        System.out.println("");
        System.out.println("Koeffizienten laenge: "+results.length);*/
        return results;
    }

    public String calculateBackward(Boolean[] koeffizientenC, Boolean[] key, int startIndex) {
        String result = "";
        Boolean[] results = new Boolean[(koeffizientenC.length - 1 + startIndex)];
        for (int i = results.length - 1; i >= 0; i--) {
            if (i - startIndex >= 0) {
                results[i] =
                        key[i - startIndex];
            } else break;
        }

        for (int j = startIndex - 1; j >= 0; j--) {
            Boolean S_nj = false;
            for (int n = koeffizientenC.length - 2; n >= 0; n--) {
                if (n != 0) { // für unbekannten s
                    if (koeffizientenC[n] == null || !koeffizientenC[n]) {
                        continue; // z.B. wenn c=0 -> x*0 = 0
                    } else {
                        S_nj = S_nj ^ results[n + j]; // XOR
                    }

                } else { // unbekannter s
                    results[j] = S_nj ^ results[j + koeffizientenC.length - 1];
                    //System.out.println("S"+j+" = "+results[j]);

                }

            }
        }
        for (int i = 0; i < results.length; i++) {
            int tmp = results[i] ? 1 : 0;
            result += String.valueOf(tmp);
        }
        return result.substring(0, koeffizientenC.length - 1);

    }
    public void feedbackPolynom(Boolean[] koeffizientenC) {
        Boolean[] p = new Boolean[koeffizientenC.length];
        for (int i = 0; i < p.length; i++) {
            p[i] = false;
        }
        for (int i = 0; i < koeffizientenC.length; i++) {
            p[koeffizientenC.length - 1 - i] = koeffizientenC[i];
        }
        String polynom = "p(x)= ";
        for (int i = p.length - 1; i >= 0; i--) {
            if (p[i] != null) {
                if (p[i]) {
                    if (i == 0) {
                        polynom += "1";
                    } else if (i == 1) {
                        polynom += "x";
                    } else {
                        polynom += "x" + "^" + i;
                    }
                    polynom += "+";
                }

            }
        }

        System.out.println(polynom.substring(0, polynom.length() - 1));
    }


}
