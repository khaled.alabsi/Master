public class ElemetsCounter {

    public int[] countDistributedElementsAsChars(String text, char[] elements) {

        int[] results = new int[elements.length];
        for (int i = 0; i < results.length; i++) {
            results[i] = 0;
        }
        for (int i = 0; i < text.length(); i++) {
            for (int j = 0; j < elements.length; j++) {
                if (text.charAt(i) == elements[j]) results[j]++;
            }

        }

        return results;
    }

    public long countDistributedElementsAsStrings(String text, String elements, boolean PrintResults) {

        int elementLenght = elements.length();
        long results = 0;
        for (int i = 0; i < text.length()-elementLenght; i++) {
            if (text.substring(i,i+elementLenght).equals(elements)) results++;
            if(PrintResults)System.out.print(text.substring(i,i+elementLenght)+"_");
        }
        if(PrintResults)System.out.println("");
        return results;
    }

}
