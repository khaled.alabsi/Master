import java.io.IOException;

public class Services {

    //diese Funktion ist nur zum Nutzer. Das Programm braucht sie nicht
    // das Programm verwendet die Funktion getKeyAsBooleanArray
    public int[] getKeyAsInt(Boolean[] clearText, Boolean[] cipherText) {

        int[] key = new int[clearText.length];
        for (int i = 0; i < clearText.length; i++) {
            key[i] = (clearText[i] ^ cipherText[i]) ? 1 : 0;
        }
        System.out.println("\nLaenge : " + key.length + " ");
        for (int i = 0; i < key.length; i++) {
            System.out.print(key[i]);
        }
        System.out.println("\n");
        return key;
    }

    public Boolean[] getKeyAsBooleanArray(Boolean[] clearText, Boolean[] cipherText) {

        Boolean[] key = new Boolean[clearText.length];
        for (int i = 0; i < clearText.length; i++) {
            key[i] = clearText[i] ^ cipherText[i];
        }
        return key;
    }




/*    public void bedingteEntropie(float X, float Y, int W_x, int W_y) {
        for (int i = 0; i < W_x; i++) {
            
        }

    }*/
}
