import java.text.DecimalFormat;
import java.util.Random;

public class TeilAufgabeB {


    public TeilAufgabeB() {
    }

    public void run() {

        //** TeilAufgabe (a
        System.out.println("***************************************************************************************************************");
        System.out.println("*                                                                                                             *");
        System.out.println("*                                   TeilAufgabe (b                                                            *");
        System.out.println("*                                                                                                             *");
        System.out.println("***************************************************************************************************************");
        System.out.println("");
        Data data = new Data();
        LFSR lfsr = new LFSR();
        ElemetsCounter elemetsCounter = new ElemetsCounter();
        Services services = new Services();
        String polynom = "x^40+x^5+x^4+x^3+1";
        System.out.println("Schritt 1: Aus dem Polynom " + polynom + " werden  dieKoeffizienten c_i raus extrahiert : ");
        Boolean[] Coefficients = lfsr.convertFeedBackPolynomToCoefficientsArray(polynom);
        System.out.println("Die Koeffizienten lautet : " + lfsr.convertBoolenArrayToString(Coefficients));
        System.out.println("");
        System.out.println("Schritt 2: Ein Random Seed generieren. ");
        StringBuilder seed = new StringBuilder();
        for (int i = 0; i < Coefficients.length - 1; i++) {
            seed.append(String.valueOf(new Random().nextInt(2)));
        }
        System.out.println("Random Seed : " + seed);
        System.out.println("");
        System.out.println("Schritt 3: LFSR mit dem koeffizienten C bilden ");
        System.out.println("");
        lfsr.build(Coefficients);
        int takt = 50000;
        System.out.println("Schritt 4: LFSR fuer " + takt + " laufenlassen");
        Boolean[] LFSRoutPut = lfsr.run(lfsr.convertStringToBoolenArray(seed.toString()), takt, 0);
        String LFSRoutPutString = lfsr.convertBoolenArrayToString(LFSRoutPut);
       // System.out.println("Ausgabe des LFSR mit " + takt + " Takt:");
       // System.out.println(LFSRoutPutString);
        System.out.println("");
        System.out.println("Schritt 5: Die Ausgabe des LFSR analysieren und Abhaengigkeitstest ausfuehren.");
        System.out.println("");
        char[] outputElements = {'0', '1'};
        int[] results = elemetsCounter.countDistributedElementsAsChars(LFSRoutPutString, outputElements);
        System.out.println("Das Bit 0 wurde " + results[0] + " aufgetaucht");
        System.out.println("Das Bit 1 wurde " + results[1] + " aufgetaucht");

//*****  Abhängigkeitstest ausführen.

        int tmpResults = (int) elemetsCounter.countDistributedElementsAsStrings(LFSRoutPutString, "11", false);
        System.out.println("Abhaengigkeitstest a=0 ,b=1");
        System.out.println("Die Kombination '11'  wurde " + tmpResults + " mals aufgetaucht");
        DecimalFormat numberFormat = new DecimalFormat("0.00");
        String term1 = numberFormat.format((((float) results[1] / (float) takt)) * ((float) results[1] / (float) takt));
        String term2 = numberFormat.format((((float) tmpResults / (float) takt)));
        System.out.println("P(b).P(b) == P(b&b) => " + term1 + " == " + term2 + " => " + term1.equals(term2));

        tmpResults = (int) elemetsCounter.countDistributedElementsAsStrings(LFSRoutPutString, "10", false);
        term1 = numberFormat.format((((float) results[1] / (float) takt)) * ((float) results[0] / (float) takt));
        term2 = numberFormat.format((((float) tmpResults / (float) takt)));
        System.out.println("Die Kombination '10'  " + tmpResults + " mals aufgetaucht");
        System.out.println("P(b).P(a) == P(b&a) => " + term1 + " == " + term2 + " => " + term1.equals(term2));


        tmpResults = (int) elemetsCounter.countDistributedElementsAsStrings(LFSRoutPutString, "10", false);
        term1 = numberFormat.format((((float) results[0] / (float) takt)) * ((float) results[1] / (float) takt));
        term2 = numberFormat.format((((float) tmpResults / (float) takt)));
        System.out.println("Die Kombination '01'  wurde " + tmpResults + " mals aufgetaucht");
        System.out.println("P(a).P(b) == P(a&b) => " + term1 + " == " + term2 + " => " + term1.equals(term2));


        tmpResults = (int) elemetsCounter.countDistributedElementsAsStrings(LFSRoutPutString, "10", false);
        term1 = numberFormat.format((((float) results[0] / (float) takt)) * ((float) results[0] / (float) takt));
        term2 = numberFormat.format((((float) tmpResults / (float) takt)));
        System.out.println("Die Kombination '00'  wurde " + tmpResults + " mals aufgetaucht");
        System.out.println("P(a).P(a) == P(a&a) => " + term1 + " == " + term2 + " => " + term1.equals(term2));

    }
}
