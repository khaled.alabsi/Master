package models;

import java.util.Objects;

public class MatcherResult {

    private int shifting;
    private int bestMatchesScore;
    private int IndexForBestMatchesScore;

    public MatcherResult() {
    }

    public void setShifting(int shifting) {
        this.shifting = shifting;
    }

    public void setBestMatchesScore(int bestMatchesScore) {
        this.bestMatchesScore = bestMatchesScore;
    }

    @Override
    public String toString() {
        return "MatcherResult{" +
                "shifting=" + shifting +
                ", bestMatchesScore=" + bestMatchesScore +
                ", IndexForBestMatchesScore=" + IndexForBestMatchesScore +
                '}';
    }

    public void setIndexForBestMatchesScore(int indexForBestMatchesScore) {
        IndexForBestMatchesScore = indexForBestMatchesScore;
    }

    public int getShifting() {
        return shifting;
    }

    public int getBestMatchesScore() {
        return bestMatchesScore;
    }

    public int getIndexForBestMatchesScore() {
        return IndexForBestMatchesScore;
    }

    public MatcherResult(int shifting, int bestMatchesScore, int indexForBestMatchesScore) {
        this.shifting = shifting;
        this.bestMatchesScore = bestMatchesScore;
        IndexForBestMatchesScore = indexForBestMatchesScore;
    }

/*    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatcherResult that = (MatcherResult) o;
        return shifting == that.shifting && bestMatchesScore == that.bestMatchesScore && IndexForBestMatchesScore == that.IndexForBestMatchesScore;
    }

    @Override
    public int hashCode() {
        return Objects.hash(shifting, bestMatchesScore, IndexForBestMatchesScore);
    }*/
}
