package services;


public class Vorgehensweise1 {

    public void run() {
        //Start
        System.out.println("***************************************************************************************************************");
        System.out.println("*                                                                                                             *");
        System.out.println("*                                   Vorgehensweise1                                                           *");
        System.out.println("*                                                                                                             *");
        System.out.println("***************************************************************************************************************");
        incrementallyCutChiffertext(Data.chiffertext, Data.fragment);
    }

    String incrementallyCutChiffertext(String chiffertext, String fragment) {
        //schritt 1
        System.out.println("Schritt 1: Der chiffertext wird inkremental abgeschnitten");
        System.out.println("           Der grosser der abgeschnitten Text ist genauso gross wie due groesser des Fragment");
        for (int i = 0; i < chiffertext.length() - fragment.length(); ++i) {
            if (i == 0)
                System.out.println("Schritt 2: Einen Key wird fuer der abgeschnitten Text und die Fragment generiert (F. generateKey() )");
            if (i == 0)
                System.out.println("Schritt 3: Wenn der abgeschnitten Text ist an der Stelle, woher die Fragment kamm,");
            if (i == 0)
                System.out.println("           dann sollte sich eine wiederholte Muster im den gefunden Key geben ");
            if (i == 0)
                System.out.println("Schritt 4: ES Wird nach eine wiederholte Muster in den gefundenen Key gesuchet (F.checkForRepeatedPattern())");
            int info = chiffertext.length() - fragment.length();
            checkForRepeatedPattern(generateKey(Data.BuchstabenMenge, chiffertext.substring(i, fragment.length() + i), fragment), 16, i, (fragment.length() + i));
        }
        return null;
    }

    //schritt 2
    String generateKey(char[] letters, String chiffertxt, String cleartext) {
        int keylenth = 3;
        String key = "";
        for (int x = 1; x <= chiffertxt.length() / keylenth; x++) {

            // chiffertxt und  cleartext in stücke teilen. kein Rest-Text wird gelassen, dafür sorgt "x+1 <= chiffertxt.length()/keylenth ?", die sich  in nächste loop anschaut.
            String chiftxt = x + 1 <= chiffertxt.length() / keylenth ? chiffertxt.substring(keylenth * (x - 1), ((keylenth) * x)) : chiffertxt.substring(keylenth * (x - 1));
            String cltxt = x + 1 <= cleartext.length() / keylenth ? cleartext.substring(keylenth * (x - 1), ((keylenth) * x)) : cleartext.substring(keylenth * (x - 1));
            for (int i = 0; i < chiftxt.length(); i++) {
                if (chiftxt.charAt(i) == cltxt.charAt(i)) {
                    key += '0';
                } else {
                    //unterschied bestimmen
                    int a = Integer.valueOf(String.valueOf(letters).indexOf(cltxt.charAt(i)));
                    int b = Integer.valueOf(String.valueOf(letters).indexOf(chiftxt.charAt(i)));
                    if (a > b) {
                        key += String.valueOf(((letters.length - a) + b) % letters.length);
                    } else {
                        key += String.valueOf(b - a);
                    }
                }
            }

        }
        return key;
    }

    //schritt 3
    private void checkForRepeatedPattern(String keyfragment, int startPatternLength, int StartIndexInChiffertxt, int EndIndexInChiffertxt) {
        // Der gesuchte Key muss sich im Keysfragment mindesten zweimal widerholen an verschiedene Stellen.
        // Der Loop schneidet einen kleinen Key (V. key) mit dem großer (V. startPatternLength) ab.
        // der geschnittene Key schiebt sich beim jeden loop um 1 (V. i)
        for (int i = 0; i < keyfragment.length() - startPatternLength; i += startPatternLength) {
            String initKey = keyfragment.substring(i, (startPatternLength + i));
            // die Java-Funktion (.indexOf()) sucht ein Treffer ab bestimmten Index und liefert der Indes des Treffer Zurück
            // die Funktion (.indexOf()) sucht sich ab Index j, die  in diesem Loop inkrementiert
            // Falls keine Treffer vorhanden ist, liefert -1 zurück. also kleine als 0
            if (keyfragment.indexOf(initKey, i) >= 0) {
                // if der Key einmal gefunden wurde, dann schau für das 2,3,,, Mal weiter
                // Man braucht hier nicht unbidengt eine zusätlche Variable(V. secondMatch)
                //So lässt es sich aber besser gelesen und verstehen
                int secondMatch = keyfragment.indexOf(initKey, i + initKey.length());

                if (keyfragment.indexOf(initKey, secondMatch + initKey.length()) > 0) {
                    int x = secondMatch + initKey.length();
                    int matches = 1;
                    while (keyfragment.indexOf(initKey, x) >= 0 && x != -1) {
                        x = +keyfragment.indexOf(initKey, x + initKey.length());
                        matches++;
                    }

                    if (matches > 5) {
                        System.out.println(" ");
                        System.out.println("Einen Treffer wurde gefunden: ");
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("Der Muster: " + initKey + " hat sich in einem Keyfragment (" + matches + ") 'mals  wiederholt! ");
                        System.out.println("Start index in Chiffretxt : " + StartIndexInChiffertxt + "  End Index: " + EndIndexInChiffertxt);
                        System.out.println(keyfragment);
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println(" ");
                        System.out.println("Schritt 5:Der Schluessel mit dem richtigen laeng wird extrahiert (F. extractKey())...");
                        String initS = extractKey(keyfragment, initKey);
                        System.out.println("Der Initiale Schluessel ist: " + initS);
                        System.out.println("suchen nach die richtige Start der Schluessel ..");
                        String initSfixidStart = findKeyStart(StartIndexInChiffertxt, initS,keyfragment);
                        System.out.println("Der Schluessel S ist: " + String.valueOf(getCorrespondingletters(Data.BuchstabenMenge, initSfixidStart)));
                        return;
                    }

                } else  // der Key wurde zweimal gefunden und jedes Mal ist er in einem anderen Hälfte der keysFragment.
                    // das heißt,dass der Key kann große sein.
                    if (keyfragment.indexOf(initKey, i) < keyfragment.length() / 2 && secondMatch > keyfragment.length() / 2) {

                        try {
                            // der große Key ist der Key + 10 extra Stellen + i, wobei der Länge  der Key und der 10 sind von mir eingeschätzt
                            String bigerKey = keyfragment.substring(i, (startPatternLength + i + 10));
                            // Man braucht hier nicht unbidengt die zusätlche Variablen(V. term1,term2)
                            //So lässt es sich aber besser gelesen und versteht werden
                            //term1: ist der big key in keyfragment vorhanden?
                            //term2: ist der big key nochmal in eine andere Stelle in keyfragment auch vorhanden?
                            boolean term1 = keyfragment.indexOf(bigerKey) > 0;
                            boolean term2 = keyfragment.indexOf(bigerKey, keyfragment.indexOf(bigerKey) + 1) > 0;

                            if (term1 && term2) {
                                System.out.println("der großere Key: " + bigerKey);
                                System.out.println("wurde zweimal gefunden");
                                System.out.println(keyfragment);
                                break;
                            }
                        } catch (StringIndexOutOfBoundsException e) {
                            continue;
                        }
                    }
            }
        }
    }

    // Liefert aus einem Langem Keyfragment einem key, der sich in diesem Keyfragment wiederholt
    String extractKey(String keyfragment, String key) {
        String tmpKey1, tmpKey2;
        int i = 0;
        while (true) {
            tmpKey1 = keyfragment.substring(0, key.length() + i);
            tmpKey2 = keyfragment.substring(tmpKey1.length(), tmpKey1.length() * 2);
            if (tmpKey1.equals(tmpKey2)) {
                return tmpKey1;
            } else {
                i++;
            }
        }
    }

    // Liefert von einem zahlfolge eine entsprechende Buchstabenlänge
    char[] getCorrespondingletters(char[] letters, String key) {
        char[] tmp = new char[key.length()];
        String str = String.valueOf(letters);
        for (int i = 0; i < key.length(); i++) {
            tmp[i] = str.charAt(Integer.parseInt(String.valueOf(key.charAt(i))));
        }
        return tmp;
    }


    String findKeyStart(int startIndexForKeyfragment, String initS,String keyfragment) {
        // Man braucht hier nicht unbidengt die zusätlche Variablen(V. fromChiffertextStartToStartIndex,keyIsCutetFrom,ignoreFirstXindex)
        //So lässt es sich aber besser gelesen und versteht werden
        int keyIsCutetFrom = startIndexForKeyfragment % initS.length();
        int ignoreFirstXindex = initS.length() - keyIsCutetFrom;
        //System.out.println("Der Schluessel ist um : " + keyIsCutetFrom + " Stellen nach Links verschoben");
        String key =  keyfragment.substring(ignoreFirstXindex, ignoreFirstXindex+initS.length());
        System.out.println("Der richtige Schluessel mit dem richten Start " + key);
        return key;

    }

}
