package models;

public class Result {
    Double durchschnitt;
    Double standardAbweichung;
    Long validTabels;
    Long NettoohneBedienung;
    Long NettounterBedienung;
    double BitsFromA;
    double BitsFromB;
    long repeatedTables;
    Integer b;
    Integer rounds;

    public Result(Double durchschnitt, Double standardAbweichung, Long validTabels, Long nettoohneBedienung, Long nettounterBedienung, Float bitsFromA, Float bitsFromB) {
        this.durchschnitt = durchschnitt;
        this.standardAbweichung = standardAbweichung;
        this.validTabels = validTabels;
        NettoohneBedienung = nettoohneBedienung;
        NettounterBedienung = nettounterBedienung;
        BitsFromA = bitsFromA;
        BitsFromB = bitsFromB;
    }

    public Result() {
    }

    public Result(Double durchschnitt, Double standardAbweichung) {
        this.durchschnitt = durchschnitt;
        this.standardAbweichung = standardAbweichung;
    }

    public Result(Double durchschnitt, Double standardAbweichung, Long validTabels, Long nettoohneBedienung, Long nettounterBedienung,
                  double bitsFromA, long repeatedTables) {
        this.durchschnitt = durchschnitt;
        this.standardAbweichung = standardAbweichung;
        this.validTabels = validTabels;
        NettoohneBedienung = nettoohneBedienung;
        NettounterBedienung = nettounterBedienung;
        BitsFromA = bitsFromA;
        this.repeatedTables = repeatedTables;
    }

    public void setResultProps(double bitsFromB, Integer b, Integer rounds) {
        BitsFromB = bitsFromB;
        this.b = b;
        this.rounds = rounds;
    }

    public void setDurchschnitt(Double durchschnitt) {
        this.durchschnitt = durchschnitt;
    }

    public void setStandardAbweichung(Double standardAbweichung) {
        this.standardAbweichung = standardAbweichung;
    }

    public void setValidTabels(Long validTabels) {
        this.validTabels = validTabels;
    }

    public void setNettoohneBedienung(Long nettoohneBedienung) {
        NettoohneBedienung = nettoohneBedienung;
    }

    public void setNettounterBedienung(Long nettounterBedienung) {
        NettounterBedienung = nettounterBedienung;
    }

    public void setBitsFromA(
            double bitsFromA) {
        BitsFromA = bitsFromA;
    }

    public void setBitsFromB(
            double bitsFromB) {
        BitsFromB = bitsFromB;
    }

    public Double getDurchschnitt() {
        return durchschnitt;
    }

    public Double getStandardAbweichung() {
        return standardAbweichung;
    }

    public Long getValidTabels() {
        return validTabels;
    }

    public Long getNettoohneBedienung() {
        return NettoohneBedienung;
    }

    public Long getNettounterBedienung() {
        return NettounterBedienung;
    }

    public double getBitsFromA() {
        return BitsFromA;
    }

    public double getBitsFromB() {
        return BitsFromB;
    }

    @Override
    public String toString() {
        return "Results{" +
                "durchschnitt=" + durchschnitt +
                ", standardAbweichung=" + standardAbweichung +
                ", validTabels=" + validTabels +
                ", NettoohneBedienung=" + NettoohneBedienung +
                ", NettounterBedienung=" + NettounterBedienung +
                ", BitsFromA=" + BitsFromA +
                ", BitsFromB=" + BitsFromB +
                '}';
    }

    public long getRepeatedTables() {
        return repeatedTables;
    }

    public void setRepeatedTables(long repeatedTables) {
        this.repeatedTables = repeatedTables;
    }

    public Integer getB() {
        return b;
    }

    public Integer getRounds() {
        return rounds;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public void setRounds(Integer rounds) {
        this.rounds = rounds;
    }
}
