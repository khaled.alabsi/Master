package models;

import java.util.Objects;

public class Tabellenspalte {

    char elementIneErsteReihe;
    char elementIneZweiteReihe;

    public Tabellenspalte(char elementIneErsteReihe, char elementIneZweiteReihe) {
        this.elementIneErsteReihe = elementIneErsteReihe;
        this.elementIneZweiteReihe = elementIneZweiteReihe;
    }

    public void setElementIneErsteReihe(char elementIneErsteReihe) {
        this.elementIneErsteReihe = elementIneErsteReihe;
    }

    public void setElementIneZweiteReihe(char elementIneZweiteReihe) {
        this.elementIneZweiteReihe = elementIneZweiteReihe;
    }

    public char getElementIneErsteReihe() {
        return elementIneErsteReihe;
    }

    public char getElementIneZweiteReihe() {
        return elementIneZweiteReihe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tabellenspalte that = (Tabellenspalte) o;
        return elementIneErsteReihe == that.elementIneErsteReihe && elementIneZweiteReihe == that.elementIneZweiteReihe;
    }

    @Override
    public int hashCode() {
        return Objects.hash(elementIneErsteReihe, elementIneZweiteReihe);
    }

    @Override
    public String toString() {
        return "Tabellenspalte{" +
                "elementIneErsteReihe=" + elementIneErsteReihe +
                ", elementIneZweiteReihe=" + elementIneZweiteReihe +
                '}';
    }
}
