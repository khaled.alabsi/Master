package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Tabelle {

    List<Tabellenspalte> tabelle = new ArrayList<>();
    //Testen ob zulässig: nein
    boolean valid = false;

    public Tabelle( List<Tabellenspalte> tabelle) {

        this.tabelle = tabelle;
    }

    public Tabelle() {
    }



    public void setTabelle(List<Tabellenspalte> tabelle) {
        this.tabelle = tabelle;
    }



    public List<Tabellenspalte> getTabelle() {
        return tabelle;
    }

    @Override
    public String toString() {
        StringBuilder ersteReihe = new StringBuilder();
        StringBuilder zweiteReihe = new StringBuilder();
        StringBuilder spaltertyp1 = new StringBuilder();
        StringBuilder spaltertyp2 = new StringBuilder();
        StringBuilder Tabelle = new StringBuilder();
        tabelle.forEach(e -> {
            ersteReihe.append("| " + e.getElementIneErsteReihe() + " ");
            zweiteReihe.append("| " + e.getElementIneZweiteReihe() + " ");
            spaltertyp1.append(" ___");
            spaltertyp2.append("----");
        });
        ersteReihe.append("|");
        zweiteReihe.append("|");
        spaltertyp2.append("-");
        Tabelle.append("Tabelle-ID: "+hashCode()+"\n");
        Tabelle.append(spaltertyp1+"\n");
        Tabelle.append(ersteReihe+"\n");
        Tabelle.append(spaltertyp2+"\n");
        Tabelle.append(zweiteReihe+"\n");
        Tabelle.append(spaltertyp1+"\n");
        return Tabelle.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tabelle tabelle1 = (Tabelle) o;
        return Objects.equals(tabelle, tabelle1.tabelle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tabelle);
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }
}
