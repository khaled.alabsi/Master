import models.Result;
import models.Tabelle;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AufgabeA {
    public static long netto;

    //eine Funktion , die zufällige Substitutionstabellen erzeugt ohne weitere Einschränkungen.
    public static Result generateRandomKeySamples(int b, int nummberOfSamples, boolean showTablesInfo, boolean showDistribution, int iter) {
        if (b < 2) {
            System.out.println("b darf nicht < als 2");
            return null;
        }
        List<Tabelle> tabellen = new ArrayList<>();
        long repeatedTables = 0;
        char[] alphabets = new char[b];
        for (int i = 0; i < b; i++) alphabets[i] = "abcdefghijklmnopqrstuvwxyz".toCharArray()[i];
        for (int i = 0; i < nummberOfSamples; i++) {
            Tabelle tb = Services.generateRandomTables(alphabets);
            if (tabellen.contains(tb)) repeatedTables++;
            tabellen.add(tb);
        }
        long vaild = howManyAreValid(tabellen);
        netto = (howManyAreValid(tabellen.stream().distinct().collect(Collectors.toList())));
        if(showTablesInfo){
            tabellen.stream().forEach(e -> System.out.println(e));
            System.out.println("Der Anzahl aller Tabellen: " + tabellen.size());
            System.out.println(" davon sind:: " + vaild + " zulässig");
            System.out.println(" davon wiederholen sich " + repeatedTables + " Tabellen");
            System.out.println(" Netto ohne Bedienung " + (tabellen.size() - repeatedTables) + " Tabellen");
            System.out.println(" Netto unter Bedienung " + netto + " Tabellen");
        }

        Result distributionResult = Services.showDistribution(tabellen,showDistribution);

        return new Result( distributionResult.getDurchschnitt(),  distributionResult.getStandardAbweichung(),
                vaild,  (long) (tabellen.size() - repeatedTables),  netto, Services.log2(((double)vaild/(double)iter)*(Services.fakultaet(b)) ),  repeatedTables);
    }


    private static int howManyAreValid(List<Tabelle> tabellen) {
        return tabellen.stream().filter(e -> e.isValid()).collect(Collectors.toList()).size();
    }


}
