package views;

import models.Result;

import java.text.DecimalFormat;
import java.util.List;

public class Consol {

    public static void showtable(List<Result> results) {
        DecimalFormat numberFormat = new DecimalFormat("0.000");

        TableList tl = new TableList(9,
                "b",
                "Ite.",
                "Valid",
              //  "Repeated",
                "Net.",
                "Net.&Valid",
              //  "S.Abweichung",
              //  "D.schnitt",
                "a=Valid/Iter",
                "log2(a*b!)",
                "b) Bits",
                "Abweich.").sortBy(1).withUnicode(true);

        results.forEach(obj -> tl.addRow(String.valueOf(obj.getB()),
                String.valueOf(obj.getRounds()),
                String.valueOf(obj.getValidTabels()),
                //String.valueOf(obj.getRepeatedTables()),
                String.valueOf(obj.getNettoohneBedienung()),
                String.valueOf(obj.getNettounterBedienung()),
                //numberFormat.format(obj.getStandardAbweichung()),
                //numberFormat.format(obj.getDurchschnitt()),
                numberFormat.format((double)obj.getValidTabels()/(double)obj.getRounds()),
                numberFormat.format(obj.getBitsFromA()),
                numberFormat.format(obj.getBitsFromB()),
                numberFormat.format((obj.getBitsFromA() - obj.getBitsFromB()))));

        tl.print();


    }
}
