public class AufgabeB {


    public static double formelB(int b, boolean showInfos) {
        double summe= 0;
        for (int i = 0; i <= b; i++) {
            double term1 = Math.pow(-1, i);
            double term2 = Services.binomi(b,  i);
            double term3 = Services.fakultaet(b-i);
            summe+=(term1*term2*term3);
        }
        if(showInfos){
            System.out.println("\n-----------------------------------\n");
            System.out.println("\nTeil (b)\n");
            System.out.println("In b) hat die Formel: "+ Services.log2(summe) +" Bits gerechnet");
            System.out.println("In a) wurde Netto "+AufgabeA.netto+" Ergebnissen gefunden. Dafür braucht man "+ Services.log2(AufgabeA.netto)+" Bits um diesen Anzahl darstellen zu können");
            if(Services.log2(summe) == Services.log2(AufgabeA.netto)) System.out.println("keine deutlische Abweichung noch!");
            System.out.println("Abweichung: "+(Services.log2(summe) - Services.log2(AufgabeA.netto)));
        }

        return Services.log2(summe);
    }


}
