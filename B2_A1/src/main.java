import models.Result;
import views.Consol;

import java.util.ArrayList;
import java.util.List;

public class main {
    public static void main(String[] args) {

        List<Result> results = new ArrayList<>();

        for (int b = 2,iter=1000; b <= 26; b++) {
            System.out.print("Wird ausgefuehrt.., bitte warten  b = "+b+"\r");
            Result result = AufgabeA.generateRandomKeySamples(b, iter, false, false,iter);//wobei 1000 ist große der Stichprobe
            result.setResultProps(AufgabeB.formelB(b, false), b, iter);
            results.add(result);
        }
        Consol.showtable(results);
        //Services.testRandForUniformDistribution(5,1000000);//wobei 5 ist der Anzahl der Buchstaben und 10000000 ist der Anzahl der Runden.
    }


}


