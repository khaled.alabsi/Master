import models.Result;
import models.Tabelle;
import models.Tabellenspalte;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Services {

    public static Tabelle generateRandomTables( char[] alphabets) {
        List<Tabellenspalte> tabelleSpalten = new ArrayList<>();
        //String schonVerwendeteChars = "";
        char[] verbleibendeAlphabets = alphabets;
        Tabelle tabelleObj = new Tabelle();
        int flag = 0;
        for (int i = 0; i < alphabets.length; i++) {
            String generateRandomalphabet = "";
            generateRandomalphabet = generateRandomalphabet(verbleibendeAlphabets);
            //Ausgeschlossen werden, die mindestens einen der Buchstaben wieder sich selbst zuordnen.
            if (!String.valueOf(alphabets[i]).equals(generateRandomalphabet)) flag++;
            // Eine neue Char Array mit nur die verbleibende Buchstaben
            verbleibendeAlphabets = getVerbleibendeAlphabets(verbleibendeAlphabets, generateRandomalphabet);
            //schonVerwendeteChars+=generateRandomalphabet;
            tabelleSpalten.add(new Tabellenspalte(alphabets[i], generateRandomalphabet.charAt(0)));
        }
        // halten alle Tabelle-Spalter die Bedienung? (Ausgeschlossen werden, die mindestens einen der Buchstaben wieder sich selbst zuordnen)
        if (flag == alphabets.length) tabelleObj.setValid(true);
        tabelleObj.setTabelle(tabelleSpalten);
        return tabelleObj;
    }

    private static char[] getVerbleibendeAlphabets(char[] verbleibendeAlphabets, String charToRemove) {
        String verbleibendeAlphabetsString = String.valueOf(verbleibendeAlphabets);
        //int indexToRemove = verbleibendeAlphabetsString.indexOf(charToRemove);
        return (new StringBuilder().append(verbleibendeAlphabetsString)).deleteCharAt(verbleibendeAlphabetsString.indexOf(charToRemove)).toString().toCharArray();
    }

    // ein Buchstabe darf nicht zweimal verwendet werden
    private static String generateRandomalphabet(char[] alphabet) {
        return String.valueOf(alphabet[new Random().nextInt(alphabet.length)]);
    }

    public static void testRandForUniformDistribution(int b, long rounds) {
        char[] alphabets = new char[b];
        for (int i = 0; i < b; i++) alphabets[i] = "abcdefghijklmnopqrstuvwxyz".toCharArray()[i];
        int[] values = new int[alphabets.length];
        String tmp = String.valueOf(alphabets);
        for (int i = 0; i < tmp.length(); i++) {
            values[i] = 0;
        }
        for (int i = 0; i <= rounds; i++) {
            values[tmp.indexOf(generateRandomalphabet(alphabets))]++;
        }
        for (int i = 0; i < tmp.length(); i++) {
            System.out.println("der Buchatabe " + alphabets[i] + " wurde " + values[i] + " Mal gewaehlt");
        }
        double average = Arrays.stream(values).average().orElse(Double.NaN);
        System.out.println("Durchschnitt: " + average);
        System.out.println("Standard Abweichung :" + sd(Arrays.stream(values).boxed().collect(Collectors.toList()), average));
    }

    public static Result showDistribution(List<Tabelle> tabellen, boolean showDistribution) {
        List<Long> values = new ArrayList<>();


        tabellen.stream()
                .collect(Collectors.groupingBy(obj -> obj.hashCode(), Collectors.counting()))
                .forEach(
                        (id, count) -> {
                            if (showDistribution) System.out.println(id + "\t\t" + count);
                            values.add(count);
                        }
                );
        Double average = values.stream().mapToDouble(val -> val).average().orElse(0.0);
        if (showDistribution) {
            System.out.println("\n-----------------------------------\n");
            System.out.println("Verteilung:\n");
            System.out.println("Die Tabellen mit den gleichen ID (Hash) haben auch die gleichen Elementen.");
            System.out.println("Innerhalb von " + tabellen.size() + " mal wurden die Tabellen wie folgt verteilt :");
            System.out.println("\n Hash \t\t   Anzahl ");
            System.out.println("Durchschnitt: " + average);
            System.out.println("Standard Abweichung :" + sd(values.stream().mapToInt(Long::intValue).boxed().collect(Collectors.toList()), average));
        }
        Result result = new Result(average, sd(values.stream().mapToInt(Long::intValue).boxed().collect(Collectors.toList()), average));
        return result;
    }

    public static double sd(List<Integer> table, double mean) {
        // Step 1:

        double temp = 0;

        for (int i = 0; i < table.size(); i++) {
            int val = table.get(i);

            // Step 2:
            double squrDiffToMean = Math.pow(val - mean, 2);

            // Step 3:
            temp += squrDiffToMean;
        }

        // Step 4:
        double meanOfDiffs = (double) temp / (double) (table.size());

        // Step 5:
        return Math.sqrt(meanOfDiffs);
    }

    static double binomi(int n, int k) {
        if ((n == k) || (k == 0))
            return 1;
        else
            return binomi(n - 1, k) + binomi(n - 1, k - 1);
    }

    public static double fakultaet(int f) {
        double ergebnis = 1;
        for (int i = 1; i <= f; i++) {
            ergebnis = ergebnis * i;
        }
        //System.out.println("Die Fakultaet von 10 ist: " + ergebnis + ".");
        return ergebnis;
    }

    public static double log2(double v) {
        return Math.log(v) / Math.log(2);
    }
}
